var createDuplicateIcon = (index) => {
  const duplicateIcon = document.createElement('img')
  duplicateIcon.src = isNightMode()
    ? '/_addons/DuplicateFieldsDev/icons/duplicate-white.png'
    : '/_addons/DuplicateFieldsDev/icons/duplicate-black.png'

  duplicateIcon.classList.add('duplicate-icon')
  duplicateIcon.id = `duplicate-icon${index}`

  duplicateIcon.addEventListener('click', () => {
    pycmd(`show_duplicates:${index}`)
  })

  return duplicateIcon
}

// can either be 'add', or 'edit'
var loadDuplicateIcons = (addMode) => {
  const fields = Array.from(document.querySelectorAll('#fields td'))
    .filter((_v, i) => i % 2 == 1)

  pycmd('check_duplicates', (dupeCounts) => {
    for (const [fieldIndex, field] of fields.entries()) {
      field.insertAdjacentElement('beforeend', createDuplicateIcon(fieldIndex))
      setDuplicateIcon(fieldIndex, dupeCounts[fieldIndex] > (addMode ? 0 : 1))
    }
  })
}

var checkFieldDuplicate = (fieldIndex, addMode) => {
  pycmd(`check_duplicate:${fieldIndex}`, (dupeCount) => {
    console.log(fieldIndex, dupeCount)
    setDuplicateIcon(fieldIndex, dupeCount > (addMode ? 0 : 1))
  })
}

var setDuplicateIcon = (fieldIndex, set) => {
  const parent = document.querySelector(`#duplicate-icon${fieldIndex}`).parentElement

  if (set) {
    parent.classList.add('has-duplicates')
  }
  else {
    parent.classList.remove('has-duplicates')
  }
}
