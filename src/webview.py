from aqt import mw, dialogs

from aqt.gui_hooks import (
    webview_will_set_content,
    webview_did_receive_js_message,
)

from aqt.editor import Editor, stripHTMLMedia
from aqt.schema_change_tracker import ChangeTracker

mw.addonManager.setWebExports(__name__, r'(web|icons)/.*\.(js|css|png)')

def load_duplicate_js(webcontent, context):
    if isinstance(context, Editor):
        addon_package = context.mw.addonManager.addonFromModule(__name__)
        base_path = f'/_addons/{addon_package}/web'

        webcontent.css.append(f'{base_path}/duplicate.css')
        webcontent.js.append(f'{base_path}/duplicate.js')

def get_query_string(fld_value, fld_name):
    query_value = fld_value.replace(r'"', r'\"')
    return f'"{fld_name}:{fld_value}"'

def check_duplicate(note, fld_index, fld_name):
    fld_value = note.fields[fld_index]
    return -1 if len(fld_value) == 0 else len(mw.col.find_notes(get_query_string(fld_value, fld_name)))

def get_fld_name_from_fld_index(flds, fld_index):
    for fld in flds:
        if fld['ord'] == fld_index:
            return fld['name']

def show_duplicates(editor, fld_index, fld_name):
    browser = dialogs.open("Browser", editor.mw)
    browser.form.searchEdit.lineEdit().setText(get_query_string(editor.note.fields[fld_index], fld_name))
    browser.onSearchActivated()

def duplicate_messages(handled, message, context: Editor):
    cmd = message.split(':', 2)

    if cmd[0] in ['check_duplicates', 'check_duplicate', 'show_duplicates']:
        mid = context.note.mid
        model = context.mw.col.models.get(mid)

        if cmd[0] == 'check_duplicates':
            dupe_values = [None] * len(context.note.fields)

            for fld in model['flds']:
                fld_index = fld['ord']
                dupe_values[fld_index] = check_duplicate(context.note, fld_index, fld['name'])

            return (True, dupe_values)

        elif cmd[0] == 'check_duplicate':
            fld_index = int(cmd[1])
            fld_name = get_fld_name_from_fld_index(model['flds'], fld_index)

            dupes = check_duplicate(context.note, fld_index, fld_name)
            return (True, dupes)

        elif cmd[0] == 'show_duplicates':
            fld_index = int(cmd[1])
            fld_name = get_fld_name_from_fld_index(model['flds'], fld_index)

            show_duplicates(context, fld_index, fld_name)
            return (True, None)

    elif cmd[0] == 'key':
        fld_index = int(cmd[1])
        add_mode = 'true' if context.addMode else 'false'

        context.web.eval(f'checkFieldDuplicate({fld_index}, {add_mode})')

    return handled

def init_webview():
    webview_will_set_content.append(load_duplicate_js)
    webview_did_receive_js_message.append(duplicate_messages)
