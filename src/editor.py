from aqt.gui_hooks import editor_will_load_note

def setup_duplicate_icons(js, note, editor):
    add_mode = 'true' if editor.addMode else 'false'
    newjs = js + f'; loadDuplicateIcons({add_mode}); '

    return newjs

def init_editor():
    editor_will_load_note.append(setup_duplicate_icons)
